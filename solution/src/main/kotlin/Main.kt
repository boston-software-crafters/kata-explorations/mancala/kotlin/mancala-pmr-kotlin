package org.bsc.katas.mancala

const val INVALID_PIT_INDEX = "Invalid argument: must be in the range 0 to 6 inclusive. Value:"
const val INVALID_PIT_COUNT = "Invalid pit count: must be in the range 1 to 48. Value:"

enum class State { Continue, Win, Draw }
sealed class Result

class ResultError(val errorMessage: String) : Result()

class ResultSuccess(val state: State, val player: Player) : Result()

class Player(val name: String) {
    var pit = intArrayOf(4, 4, 4, 4, 4, 4, 0)
}

class Game(val players: Pair<Player, Player>) {
    private var turn = 1
    var enableFreeTurn = false

    fun getTurn(): Player = if (turn % 2 == 1) players.first else players.second

    fun takeTurn(pitIndex: Int): Result {
        fun getErrorMessage(): String {
            if (pitIndex < 0 || pitIndex > 5) return "$INVALID_PIT_INDEX $pitIndex"
            val player = getTurn()
            val pitCount = player.pit[pitIndex]
            return if (pitCount < 1 || pitCount > 48) "$INVALID_PIT_COUNT $pitCount" else ""
        }

        val errorMessage = getErrorMessage()
        if (errorMessage.isNotEmpty()) return ResultError(errorMessage)
        if (enableFreeTurn) enableFreeTurn = false
        return distributeStones(pitIndex)
    }

    private fun distributeStones(pitIndex: Int): Result {
        val currentPlayer = if (turn % 2 == 1) players.first else players.second
        val otherPlayer = if (turn % 2 == 1) players.second else players.first
        val pitCount = currentPlayer.pit[pitIndex]
        currentPlayer.pit[pitIndex] = 0
        val stonesRemaining = if (pitIndex + pitCount > 12) pitIndex + pitCount - 12 else 0

        tailrec fun distributeStones(stonesRemaining: Int, startIndex: Int, stoneCount: Int) {
            fun getStonesLeft() = stonesRemaining - 12
            fun getStoneCount() = if (stonesRemaining > 12) 12 else stonesRemaining
            fun addToCurrentPlayer(n: Int) {
                for (i in startIndex until startIndex + n) currentPlayer.pit[i]++
            }

            fun addToBothPlayers() {
                fun addToOtherPlayer(n: Int) {
                    for (i in 0 until n) otherPlayer.pit[i]++
                }

                addToCurrentPlayer(7 - startIndex)
                addToOtherPlayer(stoneCount - (7 - startIndex))
            }

            fun distributeStones() {
                fun getFreeTurn() {
                    addToCurrentPlayer(stoneCount)
                    enableFreeTurn = true
                }

                fun processPotentialCapture(lastIndex: Int) {
                    fun captureStones() {
                        val otherIndex = 5 - lastIndex
                        currentPlayer.pit[6]++
                        currentPlayer.pit[lastIndex] = 0
                        currentPlayer.pit[6] += otherPlayer.pit[otherIndex]
                        otherPlayer.pit[otherIndex] = 0
                    }

                    addToCurrentPlayer(stoneCount)
                    if (currentPlayer.pit[lastIndex] == 1) captureStones()
                }

                when {
                    startIndex + stoneCount < 7 -> processPotentialCapture(startIndex + stoneCount - 1)
                    startIndex + stoneCount == 7 && stonesRemaining <= 0 -> getFreeTurn()
                    else -> addToBothPlayers()
                }
            }

            distributeStones()
            if (stonesRemaining <= 0) return
            distributeStones(getStonesLeft(), 0, getStoneCount())
        }

        fun getResult(): Result {
            fun getWinner(): Result {
                players.first.pit[6] = players.first.pit.sum()
                players.second.pit[6] = players.second.pit.sum()
                return when {
                    players.first.pit.sum() > players.second.pit.sum() -> ResultSuccess(State.Win, players.first)
                    players.first.pit.sum() == players.second.pit.sum() -> ResultSuccess(State.Draw, currentPlayer)
                    else -> ResultSuccess(State.Win, players.second)
                }
            }

            fun haveWinner() = players.first.pit.slice(0..5).sum() == 0 ||
                    players.second.pit.slice(0..5).sum() == 0

            fun getNextPlayer() = if (enableFreeTurn) currentPlayer else otherPlayer

            return if (haveWinner()) getWinner() else ResultSuccess(State.Continue, getNextPlayer())
        }

        distributeStones(stonesRemaining, pitIndex + 1, pitCount - stonesRemaining)
        if (!enableFreeTurn) turn++
        return getResult()
    }
}
