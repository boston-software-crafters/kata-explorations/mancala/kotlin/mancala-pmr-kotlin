package org.bsc.katas.mancala

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class MainTest {
    private val game by lazy {
        val first = Player("First")
        val second = Player("Second")
        val players = Pair(first, second)
        Game(players)
    }

    @Test fun `when starting out verify the environment works`() { assertEquals(4, 2 + 2) }

    @Test fun `verify creating a Game with two named players is possible`() {
        val tom = Player("tom")
        val jane = Player("jane")
        val players = Pair(tom, jane)
        val game = Game(players)
        assertEquals("tom", game.players.first.name)
        assertEquals("jane", game.players.second.name)
    }

    @Test fun `with a new game and players named first and second verify the current turn belongs to first`() {
        assertEquals("First", game.getTurn().name)
    }

    @Test fun `with a new game and players named first and second verify they take turns`() {
        game.takeTurn(0)
        assertEquals("Second", game.getTurn().name)
    }

    @Test fun `verify that pit index value outside the range 0 to 5 is reported as an error`() {
        val pit = intArrayOf(-1, 6)
        pit.forEach {
            val actual = game.takeTurn(it)
            assertTrue(actual is ResultError && actual.errorMessage.endsWith("$it"))
        }
    }

    @Test fun `verify that pit index value in the range 0 to 5 is reported as valid`() {
        val pitIndex = intArrayOf(0, 1, 2, 3, 4)
        pitIndex.forEach {
            val actual = game.takeTurn(it)
            assertTrue(actual is ResultSuccess && actual.state == State.Continue)
        }
        val actual = game.takeTurn(5)
        assertTrue(actual is ResultSuccess && actual.state == State.Continue)
    }

    @Test fun `verify that pit content value is greater than 0 and less than 48`() {
        game.players.first.pit[0] = 0
        val actual = game.takeTurn(0)
        assertTrue(actual is ResultError && actual.errorMessage.endsWith("0"))
    }

    @Test fun `verify that the first pit contents are distributed as expected`() {
        game.takeTurn(0)
        val expected = intArrayOf(0, 5, 5, 5, 5, 4, 0)
        assertTrue(game.players.first.pit.contentEquals(expected))
        assertFalse(game.enableFreeTurn)
    }

    @Test fun `verify that the second pit contents are distributed as expected without a free turn`() {
        game.takeTurn(1)
        val expected = intArrayOf(4, 0, 5, 5, 5, 5, 0)
        assertTrue(game.players.first.pit.contentEquals(expected))
        assertFalse(game.enableFreeTurn)
    }

    @Test fun `verify that the third pit contents are distributed as expected and a free turn ensues`() {
        game.takeTurn(2)
        val expected = intArrayOf(4, 4, 0, 5, 5, 5, 1)
        val actual = game.players.first.pit
        assertTrue(actual.contentEquals(expected))
        assertTrue(game.enableFreeTurn)
    }

    @Test fun `verify that the fourth pit contents are distributed as expected without a free turn`() {
        game.takeTurn(2)
        val expected = intArrayOf(4, 4, 0, 5, 5, 5, 1)
        val actual = game.players.first.pit
        assertTrue(actual.contentEquals(expected))
        assertTrue(game.enableFreeTurn)
    }

    @Test fun `when the first player has all empty pits end the game with a win for the other player`() {
        for (i in 0 .. 4) game.players.first.pit[i] = 0
        val result = game.takeTurn(5)
        val expected = intArrayOf(0, 0, 0, 0, 0, 0, 1)
        val actual = game.players.first.pit
        assertTrue(actual.contentEquals(expected))
        assertFalse(game.enableFreeTurn)
        assertTrue(result is ResultSuccess)
        assertEquals(State.Win,  result.state)
        assertEquals(game.players.second, result.player)
        assertEquals(1, game.players.first.pit[6])
        assertEquals(27, game.players.second.pit[6])
    }

    // test the capture rule
    @Test fun `when a capture opportunity is presented verify the correct mancala value`() {
        val expectedCurrent = intArrayOf(0, 4, 4, 4, 4, 0, 7)
        val expectedOther = intArrayOf(5, 5, 5, 5, 5, 0, 0)
        val actualCurrent = game.players.first.pit
        val actualOther = game.players.second.pit
        fun showArrays() = "Actual: ${actualCurrent.contentToString()}; Expected: ${expectedCurrent.contentToString()}"

        game.players.first.pit[5] = 8
        game.players.first.pit[0] = 0
        val result = game.takeTurn(5)
        assertTrue(actualCurrent.contentEquals(expectedCurrent), showArrays())
        assertTrue(actualOther.contentEquals(expectedOther))
        assertFalse(game.enableFreeTurn)
        assertTrue(result is ResultSuccess)
        assertEquals(State.Continue,  result.state)
        assertEquals(game.players.second, result.player)
    }
}
